﻿using System;
using System.Collections.Generic;
using MapGenerator.Events;
using MapGenerator.Input;
using MapGenerator.Input.Options;
using MapGenerator.Map;
using MapGenerator.Map.Generation;
using MapGenerator.Players;
using MapGenerator.Rendering;
using MapGenerator.Types;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapGenerator
{
    public class Game1 : Game
    {
        private readonly Queue<int> _seeds = new Queue<int>(new List<int>
        {
            258939968,
            242647125
        });

        private PlayerController _playerController;
        private GraphicsDeviceManager _graphics;
        private InputDetector _inputDetector;
        private SpriteBatch _spriteBatch;
        private UiRenderer _uiRenderer;
        private Renderer _renderer;
        private Camera _camera;
        private HexMap _hexMap;
        private int _seed;

        public Game1()
        {
            IsMouseVisible = true;
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            _inputDetector = new InputDetector(GraphicsDevice.Viewport, new InputOptions());

            _camera = new Camera(GraphicsDevice.Viewport);
            _camera.BindTo(_inputDetector);

            _spriteBatch = new SpriteBatch(GraphicsDevice);
            var contentFactory = ContentFactory.From(GraphicsDevice, Content);
            _renderer = new Renderer(_spriteBatch, contentFactory, _camera);
            _uiRenderer = new UiRenderer(new SpriteBatch(GraphicsDevice), contentFactory);

            var mapSize = new IndexSize(128, 128);
            var tileSize = new IndexSize(16, 16);

            _hexMap = HexMap.Generate(128, 128);

            _inputDetector.OnMouseMoved.Subscribe(moveEvent =>
            {
                _hexMap.Pick(moveEvent.WorldPosition);
            });

            _seed = GenerateSeed();
            var worldMap = IslandGenerator.Generate(mapSize, tileSize, new RandomGenerator(_seed));

            _playerController = new PlayerController(new WorldController(worldMap), new MinionController());
            _playerController.Initialise();

            _inputDetector.OnInput.Subscribe(x =>
            {
                if (x.KeySet.Contains(Keys.R))
                    DomainEvents.Publish(new CreateBuildingEvent(x.MousePosition + _camera.Position));
                
                if (x.KeySet.Contains(Keys.Space))
                {
                    _seed = GenerateSeed();
                    worldMap = IslandGenerator.Generate(mapSize, tileSize, new RandomGenerator(_seed));
                    _playerController = new PlayerController(new WorldController(worldMap), new MinionController());
                    _playerController.Initialise();
                }

                if (x.KeySet.Contains(Keys.V))
                {
                    worldMap = IslandGenerator.Generate(mapSize, tileSize, new RandomGenerator(_seed));
                    _playerController = new PlayerController(new WorldController(worldMap), new MinionController());
                    _playerController.Initialise();
                }

                if (x.KeySet.Contains(Keys.Enter))
                    DomainEvents.Publish(new MinionSpawnedEvent());

                if (x.KeySet.Contains(Keys.D1))
                    DebugConfiguration.Value.RenderGatewayNodes = !DebugConfiguration.Value.RenderGatewayNodes;

                if (x.KeySet.Contains(Keys.D2))
                    DebugConfiguration.Value.RenderRegions = !DebugConfiguration.Value.RenderRegions;

                if (x.KeySet.Contains(Keys.D3))
                    DebugConfiguration.Value.RenderClearance = !DebugConfiguration.Value.RenderClearance;

                if (x.KeySet.Contains(Keys.D4))
                    DebugConfiguration.Value.RenderGatewayNeighbours = !DebugConfiguration.Value.RenderGatewayNeighbours;

                if (x.KeySet.Contains(Keys.D5))
                    DebugConfiguration.Value.RenderGatewayPaths = !DebugConfiguration.Value.RenderGatewayPaths;
            });


            base.Initialize();
        }

        protected int GenerateSeed()
        {
            return _seeds.Count > 0 ? _seeds.Dequeue() : Environment.TickCount;
        }

        protected override void Dispose(bool disposing)
        {
            _camera.Dispose();
            base.Dispose(disposing);
        }

        protected override void LoadContent()
        {
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            _inputDetector.Update();
            _playerController.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            _renderer.Begin();
           // _playerController.RenderTo(_renderer);
           _hexMap.DrawTo(_renderer);
            _renderer.End();

            _uiRenderer.Begin();
            _uiRenderer.WriteText($"{_seed}", Vector2.Zero);
            _uiRenderer.End();
            base.Draw(gameTime);
        }
    }

    public class CreateBuildingEvent : IEvent
    {
        public Position MousePosition { get; }

        public CreateBuildingEvent(Position mousePosition)
        {
            MousePosition = mousePosition;
        }
    }
}