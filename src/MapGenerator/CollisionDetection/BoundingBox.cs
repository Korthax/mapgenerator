﻿using System;
using MapGenerator.Types;

namespace MapGenerator.CollisionDetection
{
    public class BoundingBox
    {
        public Position Position { get; }
        public Size Size { get; }

        public BoundingBox(Position position, Size size)
        {
            Position = position;
            Size = size;
        }

        public bool Intersects(BoundingBox b)
        {
            return Math.Abs(Position.X - b.Position.X) * 2 < Size.Width + b.Size.Width &&
                   Math.Abs(Position.Y - b.Position.Y) * 2 < Size.Height + b.Size.Height;
        }
    }
}