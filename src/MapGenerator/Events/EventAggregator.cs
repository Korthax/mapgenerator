﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace MapGenerator.Events
{
    public class EventAggregator
    {
        private readonly CompositeDisposable _subscriptions;
        private readonly Subject<IEvent> _subject;
        private bool _disposed;

        public EventAggregator()
        {
            _subject = new Subject<IEvent>();
            _subscriptions = new CompositeDisposable();
        }

        public void Register<TEvent>(IEventHandler<TEvent> eventHandler) where TEvent : IEvent
        {
           var subscription = _subject
                .OfType<TEvent>()
                .AsObservable()
                .Subscribe(eventHandler.Handle);

            _subscriptions.Add(subscription);
        }

        public void Publish<TEvent>(TEvent domainEvent) where TEvent : IEvent
        {
            _subject.OnNext(domainEvent);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _subscriptions.Dispose();
            _subject.Dispose();
            _disposed = true;
        }
    }
}