﻿using System;
using System.Threading;

namespace MapGenerator.Events
{
    public static class DomainEvents
    {
        private static readonly Lazy<EventAggregator> EventAggregator;

        static DomainEvents()
        {
            EventAggregator = new Lazy<EventAggregator>(() => new EventAggregator(), LazyThreadSafetyMode.ExecutionAndPublication);
        }

        public static void Register<TEvent>(IEventHandler<TEvent> eventHandler) where TEvent : IEvent
        {
            EventAggregator.Value.Register(eventHandler);
        }

        public static void Publish<TEvent>(TEvent domainEvent) where TEvent : IEvent
        {
            EventAggregator.Value.Publish(domainEvent);
        }

        public static void Stop()
        {
            EventAggregator.Value.Dispose();
        }
    }
}