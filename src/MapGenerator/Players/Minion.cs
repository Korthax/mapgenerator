﻿using System;
using MapGenerator.Map.PathFinding;
using MapGenerator.Rendering;
using MapGenerator.Types;
using Microsoft.Xna.Framework;

namespace MapGenerator.Players
{
    public class Minion
    {
        private readonly SteeringBehaviour _steeringBehaviour;
        private readonly Path _path;

        private Position _position;
        private Position _target;
        private Vector _velocity;
        private float _rotation;

        public Minion(Path path, SteeringBehaviour steeringBehaviour)
        {
            _path = path;
            _steeringBehaviour = steeringBehaviour;
            _position = _path.Next();
            _target = _path.Next();
            _velocity = Vector.Zero;
        }

        public void Update()
        {
            if (Position.Distance(_position, _target) < 8 && _path.Count > 0)
                _target = _path.Next();

            var steering = Seek(_target, _position);
            var steeringForce = Truncate(steering, _steeringBehaviour.MaxForce);
            var acceleration = steeringForce / _steeringBehaviour.Mass;

            _velocity = Truncate(_velocity + acceleration, _steeringBehaviour.MaxSpeed);
            _rotation = Rotate();
            _position += _velocity;
        }

        public static Vector Truncate(Vector original, float max)
        {
            if(original.Length() <= max)
                return original;

            original.Normalise();
            original *= max;

            return original;
        }

        private Vector Seek(Position heading, Position position)
        {
            var difference = heading - position;
            if (difference.Equals(Position.Zero))
                return Vector.Zero;

            if (Math.Abs(difference.Length() - 0.0f) < float.Epsilon)
                return Vector.Zero;

            return new Vector(difference.Normalise() * _steeringBehaviour.MaxSpeed) - _velocity;
        }

        private float Rotate()
        {
            if (_velocity != Vector.Zero && Position.Distance(_position, _target) > 0.1f)
                return (float)Math.Atan2(-_velocity.Y, _velocity.X);

            return _rotation;
        }

        public void RenderTo(Renderer renderer)
        {
            renderer.RenderDot(_position, new IndexSize(15, 15), Color.Red);
        }
    }
}