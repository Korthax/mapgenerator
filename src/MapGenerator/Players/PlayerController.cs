﻿using MapGenerator.Events;
using MapGenerator.Map;
using MapGenerator.Rendering;
using Microsoft.Xna.Framework;

namespace MapGenerator.Players
{
    public class PlayerController : IEventHandler<MinionSpawnedEvent>, IEventHandler<CreateBuildingEvent>
    {
        private readonly MinionController _minionController;
        private readonly WorldController _worldController;

        public PlayerController(WorldController worldController, MinionController minionController)
        {
            _worldController = worldController;
            _minionController = minionController;
        }

        public void Handle(MinionSpawnedEvent domainEvent)
        {
            var routeInfo = _worldController.GetRouting();
            _minionController.Create(routeInfo);
        }

        public void Handle(CreateBuildingEvent domainEvent)
        {
            _worldController.AddBuilding(domainEvent.MousePosition);
        }

        public void Initialise()
        {
            DomainEvents.Register<MinionSpawnedEvent>(this);
            DomainEvents.Register<CreateBuildingEvent>(this);
        }

        public void RenderTo(Renderer renderer)
        {
            _worldController.RenderTo(renderer);
            _minionController.RenderTo(renderer);
        }

        public void Update(GameTime gameTime)
        {
            _minionController.Update(gameTime);
        }
    }
}