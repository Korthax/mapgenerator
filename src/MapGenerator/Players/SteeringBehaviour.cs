﻿namespace MapGenerator.Players
{
    public class SteeringBehaviour
    {
        public float MaxForce { get; set; }
        public float MaxSpeed { get; set; }
        public float Mass { get; set; }
    }
}