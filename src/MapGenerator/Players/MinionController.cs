﻿using System.Collections.Generic;
using MapGenerator.Map.PathFinding;
using MapGenerator.Rendering;
using Microsoft.Xna.Framework;

namespace MapGenerator.Players
{
    public class MinionController
    {
        private readonly List<Minion> _minions;

        public MinionController()
        {
            _minions = new List<Minion>();
        }

        public Minion Create(RouteInfo routeInfo)
        {
            var minion = new Minion(routeInfo.BuildPath(), new SteeringBehaviour { Mass = 1, MaxForce = 1, MaxSpeed = 1 });
            _minions.Add(minion);
            return minion;
        }

        public void Update(GameTime gameTime)
        {
            foreach(var minion in _minions)
                minion.Update();
        }

        public void RenderTo(Renderer renderer)
        {
            foreach(var minion in _minions)
                minion.RenderTo(renderer);
        }
    }
}