using System;

namespace MapGenerator.Types
{
    public class IndexSize
    {
        public static IndexSize Zero => new IndexSize(0, 0); 
        public int Width { get; }
        public int Height { get; }

        public IndexSize(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public bool Contains(Index index)
        {
            return 0 <= index.X && 0 <= index.Y && index.X < Width && index.Y < Height;
        }

        public static IndexSize operator *(IndexSize size, float scale)
        {
            return new IndexSize((int)(size.Width * scale), (int)(size.Height * scale));
        }

        public static Size operator *(IndexSize size, Size scale)
        {
            return new Size(size.Width * scale.Width, size.Height * scale.Height);
        }

        public static IndexSize operator *(IndexSize size, IndexSize scale)
        {
            return new IndexSize(size.Width * scale.Width, size.Height * scale.Height);
        }

        public static IndexSize operator /(IndexSize size, float scale)
        {
            return new IndexSize((int)(size.Width / scale), (int)(size.Height / scale));
        }

        public static bool operator ==(IndexSize a, IndexSize b)
        {
            var aIsNull = ReferenceEquals(a, null);
            var bIsNull = ReferenceEquals(b, null);

            if (aIsNull && bIsNull)
                return true;

            if (aIsNull || bIsNull)
                return false;

            return Math.Abs(a.Width - b.Width) < float.Epsilon && Math.Abs(a.Height - b.Height) < float.Epsilon;
        }

        public static bool operator !=(IndexSize a, IndexSize b)
        {
            return !(a == b);
        }

        public bool Equals(IndexSize other)
        {
            return Width.Equals(other.Width) && Height.Equals(other.Height);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            return obj is IndexSize size && Equals(size);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Width.GetHashCode() * 397) ^ Height.GetHashCode();
            }
        }

        public Size ToSize()
        {
            return new Size(Width, Height);
        }
    }
}