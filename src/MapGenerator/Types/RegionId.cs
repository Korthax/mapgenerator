namespace MapGenerator.Types
{
    public class RegionId
    {
        public int X => _index.X;
        public int Y => _index.Y;
        public int Value { get; }

        private readonly Index _index;

        public static RegionId From(Index index, IndexSize size)
        {
            return new RegionId(index, index.X + index.Y * size.Width);
        }

        private RegionId(Index index, int value)
        {
            _index = index;
            Value = value;
        }
    }
}