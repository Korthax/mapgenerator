﻿using System;
using MapGenerator.Types;

namespace MapGenerator.Extensions
{
    public static class ArrayExtensions
    {
        public static IndexSize Size<T>(this T[,] self)
        {
            return new IndexSize(self.Width(), self.Height());
        }

        public static int Width<T>(this T[,] self)
        {
            return self.GetUpperBound(0) + 1;
        }

        public static int Height<T>(this T[,] self)
        {
            return self.GetUpperBound(1) + 1;
        }
    }
}
