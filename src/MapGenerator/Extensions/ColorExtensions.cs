﻿using Microsoft.Xna.Framework;

namespace MapGenerator.Extensions
{
    public static class ColorExtensions
    {
        public static Color MakeTranslucent(this Color color, float alpha)
        {
            return new Color(color, alpha);
        }
    }
}