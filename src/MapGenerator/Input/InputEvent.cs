﻿using System.Collections.Generic;
using MapGenerator.Types;
using Microsoft.Xna.Framework.Input;

namespace MapGenerator.Input
{
    public class MouseMoveEvent
    {
        public Position MousePosition { get; }
        public Position WorldPosition { get; }

        public static MouseMoveEvent From(Position mousePosition, Position screenCentre)
        {
            return new MouseMoveEvent(mousePosition, mousePosition + screenCentre);
        }

        private MouseMoveEvent(Position mousePosition, Position worldPosition)
        {
            MousePosition = mousePosition;
            WorldPosition = worldPosition;
        }
    }

    public class InputEvent
    {
        public Position MousePosition { get; private set; }
        public ModifierKeys Modifiers { get; private set; }
        public HashSet<Keys> KeySet { get; private set; }
        public HashSet<MouseButtons> ButtonSet { get; private set; }

        public static InputEvent From(HashSet<Keys> keys, HashSet<MouseButtons> buttons, Position mousePosition)
        {
            var modifiers = ModifierKeys.None;

            if (keys.Contains(Keys.LeftAlt))
                modifiers |= ModifierKeys.LeftAlt;

            if (keys.Contains(Keys.RightAlt))
                modifiers |= ModifierKeys.RightAlt;

            if (keys.Contains(Keys.LeftShift))
                modifiers |= ModifierKeys.LeftShift;

            if (keys.Contains(Keys.RightShift))
                modifiers |= ModifierKeys.RightShift;

            if (keys.Contains(Keys.LeftControl))
                modifiers |= ModifierKeys.LeftControl;

            if (keys.Contains(Keys.RightControl))
                modifiers |= ModifierKeys.RightControl;

            return new InputEvent
            {
                MousePosition = mousePosition,
                Modifiers = modifiers,
                ButtonSet = buttons,
                KeySet = keys
            };
        }
    }
}