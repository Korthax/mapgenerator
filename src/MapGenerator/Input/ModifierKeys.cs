﻿using System;

namespace MapGenerator.Input
{
    [Flags]
    public enum ModifierKeys : uint
    {
        None = 0,
        LeftAlt = 1,
        RightAlt = 2,
        LeftShift = 4,
        RightShift = 8,
        LeftControl = 16,
        RightControl = 32,
    }
}