﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using MapGenerator.Input.Extensions;
using MapGenerator.Input.Options;
using MapGenerator.Types;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapGenerator.Input
{
    public interface IInputDetector
    {
        IObservable<InputEvent> OnInput { get; }
        IObservable<MouseMoveEvent> OnMouseMoved { get; }

        void Update();
    }

    public class InputDetector : IInputDetector
    {
        public IObservable<MouseMoveEvent> OnMouseMoved => _onMouseMoved;
        public IObservable<InputEvent> OnInput => _onInput;

        private readonly Subject<MouseMoveEvent> _onMouseMoved;
        private readonly Subject<InputEvent> _onInput;
        private readonly InputOptions _inputOptions;
        private readonly Position _screenCentre;

        private HashSet<MouseButtons> _oldDownButtons;
        private HashSet<Keys> _oldDownKeys;
        private Position _mousePosition;

        public InputDetector(Viewport viewport, InputOptions inputOptions)
        {
            _screenCentre = new Position(viewport.Width / 2f, viewport.Height / 2f);
            _inputOptions = inputOptions;
            _onMouseMoved = new Subject<MouseMoveEvent>();
            _onInput = new Subject<InputEvent>();
            _oldDownKeys = new HashSet<Keys>();
            _oldDownButtons = new HashSet<MouseButtons>();
            _mousePosition = new Position(Mouse.GetState().X, Mouse.GetState().Y);
           // _mousePosition = _screenCentre;
            //Mouse.SetPosition((int)_screenCentre.X, (int)_screenCentre.Y);
        }

        public void Update()
        {
            var newKeyboardState = Keyboard.GetState();
            var newMouseState = Mouse.GetState();

            var mouseMovement = _screenCentre - new Position(newMouseState.X, newMouseState.Y);
            _mousePosition -= mouseMovement;

            var downKeys = new HashSet<Keys>(newKeyboardState.GetPressedKeys());
            var downButtons = new HashSet<MouseButtons>(newMouseState.GetPressedButtons());

            if(_inputOptions.Type == InputType.OnKeyUp)
            {
                var releasedKeys = new HashSet<Keys>(_oldDownKeys.Where(x => !downKeys.Contains(x)));
                var releasedButtons = new HashSet<MouseButtons>(_oldDownButtons.Where(x => !downButtons.Contains(x)));

                if(releasedKeys.Count > 0 || releasedButtons.Count > 0)
                    _onInput.OnNext(InputEvent.From(releasedKeys, releasedButtons, _mousePosition));
            }
            else
            {
                if (downKeys.Count > 0 || downButtons.Count > 0)
                    _onInput.OnNext(InputEvent.From(downKeys, downButtons, _mousePosition));
            }


            if(mouseMovement != Position.Zero)
            {
                _onMouseMoved.OnNext(MouseMoveEvent.From(_mousePosition, _screenCentre));
                Mouse.SetPosition((int)_screenCentre.X, (int)_screenCentre.Y);
            }

            _oldDownKeys = downKeys;
            _oldDownButtons = downButtons;
        }
    }
}