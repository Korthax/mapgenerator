﻿using System;
using Microsoft.Xna.Framework;

namespace MapGenerator.Map
{
    public static class NodeTypeExtensions
    {
        public static bool IsLiquid(this NodeType nodeType)
        {
            var value = (int)nodeType;
            return 0 < value && value < 50;
        }

        public static bool IsLand(this NodeType nodeType)
        {
            var value = (int)nodeType;
            return 50 <= value;
        }

        public static bool IsLowland(this NodeType nodeType)
        {
            var value = (int)nodeType;
            return 50 <= value && value < 75;
        }

        public static bool IsHighland(this NodeType nodeType)
        {
            var value = (int)nodeType;
            return 75 <= value && value < 100;
        }

        public static Color ToColour(this NodeType nodeType)
        {
            switch (nodeType)
            {
                case NodeType.None:
                    return Color.Transparent;
                case NodeType.Ocean:
                    return Color.Blue;
                case NodeType.DeepOcean:
                    return Color.DarkBlue;
                case NodeType.Ice:
                    return Color.Turquoise;
                case NodeType.Lake:
                    return Color.CornflowerBlue;
                case NodeType.Grassland:
                    return Color.Green;
                case NodeType.Mountains:
                    return Color.Gray;
                case NodeType.Hill:
                    return Color.LightGray;
                case NodeType.Coast:
                    return Color.SandyBrown;
                case NodeType.Forest:
                    return Color.DarkGreen;
                case NodeType.Dirt:
                    return Color.SaddleBrown;
                default:
                    throw new ArgumentOutOfRangeException(nameof(nodeType), nodeType, null);
            }
        }
    }
}