﻿using System;
using System.Collections;
using System.Collections.Generic;
using MapGenerator.Extensions;
using MapGenerator.Types;
using Microsoft.Xna.Framework;

namespace MapGenerator.Map
{
    public class OldHexPosition
    {
        private static readonly Index[][] Directions = {
            new[]
            {
                new Index(+1, 0), new Index(+1, -1), new Index(0, -1),
                new Index(-1, 0), new Index(0, +1), new Index(+1, +1)
            },
            new[]
            {
                new Index(+1, 0), new Index(0, -1), new Index(-1, -1),
                new Index(-1, 0), new Index(-1, +1), new Index(0, +1)
            }
        };

        public Index Index { get; }
        public Position Position { get; }

        private readonly IndexSize _size;

        public static OldHexPosition FromOffset(Index index, IndexSize size)
        {
            return From(index.X, index.Y, size);
        }

        public static OldHexPosition From(int col, int row, IndexSize size)
        {
            var x = size.Width / 2f * (float)Math.Sqrt(3) * (col - 0.5f * (row % 2));
            var y = size.Height / 2f * (3 / 2f) * row;
            return new OldHexPosition(new Index(col, row), new Position(x, y), size);
        }

        public static OldHexPosition operator +(OldHexPosition left, OldHexPosition right)
        {
            return FromOffset(left.Index + right.Index, left._size);
        }

        private OldHexPosition(Index index, Position position, IndexSize size)
        {
            _size = size;
            Index = index;
            Position = position;
        }

        public OldHexPosition Direction(int direction)
        {
            return FromOffset(Index + Directions[Index.Y & 1][direction], _size);
        }

        protected bool Equals(OldHexPosition other)
        {
            return Equals(Index, other.Index);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((OldHexPosition)obj);
        }

        public override int GetHashCode()
        {
            return Index != null ? Index.GetHashCode() : 0;
        }
    }

    public class NodeMap : IEnumerable<Node>
    {
        private readonly Dictionary<Index, Node> _nodes;
        private readonly IndexSize _size;

        public static NodeMap From(IndexSize size)
        {
            return new NodeMap(new Dictionary<Index, Node>(), size);
        }

        private NodeMap(Dictionary<Index, Node> nodes, IndexSize size)
        {
            _nodes = nodes;
            _size = size;
        }

        public void Add(Node node)
        {
            _nodes.Add(node.Hex.Index, node);
        }

        public IndexSize Size()
        {
            return _size;
        }

        public IEnumerator<Node> GetEnumerator()
        {
            return _nodes.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Node Get(OldHexPosition hex)
        {
            return _nodes[hex.Index];
        }

        public void SetElevation(OldHexPosition hex, int elevation)
        {
            _nodes[hex.Index].Elevation = elevation;
        }

        public Node GetNeighbour(OldHexPosition hex, int direction)
        {
            var dir = hex.Direction(direction);
            return Get(dir);
        }

        public List<Node> GetNeighbours(OldHexPosition hex, int radius)
        {
            var items = new List<Node>();

            for (var k = 1; k <= radius; k++)
            {
                var cube = hex;
                for (var i = 0; i < k; i++)
                    cube = cube.Direction(4);

                for (var i = 0; i < 6; i++)
                {
                    for (var j = 0; j < k; j++)
                    {
                        if (!_nodes.ContainsKey(cube.Index))
                        {
                            cube = cube.Direction(i);
                            continue;
                        }

                        items.Add(Get(cube));
                        cube = cube.Direction(i);
                    }
                }
            }

            return items;
        }
    }

    public class Node
    {
        public static int NumberOfNeighbours = 6;

        public static readonly Index[][] Directions = {
            new[]
            {
                new Index(+1, 0), new Index(+1, -1), new Index(0, -1),
                new Index(-1, 0), new Index(0, +1), new Index(+1, +1)
            },
            new[]
            {
                new Index(+1, 0), new Index(0, -1), new Index(-1, -1),
                new Index(-1, 0), new Index(-1, +1), new Index(0, +1)
            }
        };

        public int Elevation { get; set; }
        public NodeType Type { get; set; }
        public OldHexPosition Hex { get; }
        public int Clearance { get; set; }
        public Color? ColourOverride { get; set; }
        public Index RegionPosition { get; set; }
        public RegionId RegionId { get; set; }
        public int IslandId { get; set; }

        public Node(OldHexPosition position)
        {
            Hex = position;
            IslandId = -1;
        }

        public Node GetNeighbour(int direction, NodeMap graph)
        {
            return graph.GetNeighbour(Hex, direction);
        }

        public List<Node> GetNeighbours(int radius, NodeMap graph)
        {
            return graph.GetNeighbours(Hex, radius);
        }

        protected bool Equals(Node other)
        {
            return Equals(Hex, other.Hex);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((Node)obj);
        }

        public override int GetHashCode()
        {
            return Hex != null ? Hex.GetHashCode() : 0;
        }

        public double CostFrom(Node start)
        {
            if (start.Clearance < Clearance)
                return 1;

            if (Clearance == start.Clearance)
                return 5;

            return 8;
        }
    }
}