﻿using System;
using System.Collections.Generic;
using MapGenerator.Map.Generation;
using MapGenerator.Map.PathFinding;
using MapGenerator.Rendering;
using MapGenerator.Types;
using Microsoft.Xna.Framework;

namespace MapGenerator.Map
{
    public class WorldMap
    {
        public readonly IndexSize Bounds;

        private readonly List<Position> _resources;
        private readonly SpawnPoints _spawnPoints;
        private readonly IPathFinder _pathFinder;
        private readonly IndexSize _tileSize;
        private readonly HexMap _hexMap;
        private readonly Route _route;

        public WorldMap(HexMap hexMap, IndexSize size, IndexSize tileSize, List<Position> resources, SpawnPoints spawnPoints, IPathFinder pathFinder)
        {
            _hexMap = hexMap;
            _tileSize = tileSize;
            _resources = resources;
            _spawnPoints = spawnPoints;
            _pathFinder = pathFinder;
            Bounds = size * tileSize;
            _route = GetRoute();
        }

        public void DrawTo(Renderer renderer)
        {
            _hexMap.DrawTo(renderer);

            foreach(var resource in _resources)
                renderer.Render(new Position(resource.X, resource.Y), _tileSize, Color.Red);

            foreach(var node in _route)
                renderer.RenderOutline(node.Hex.Position, _tileSize, Color.Orange);

            _spawnPoints.DrawTo(renderer, _tileSize);
        }

        public Route GetRoute()
        {
            return new Route(new List<Node>());
            //return _pathFinder.Search(_hexMap, _spawnPoints.Enemy.Hex, _spawnPoints.Player.Hex, 2);
        }

        public void AddBuilding(Position position)
        {
           // var t = Hex.PixelToHex(_tileSize, position);
            //_hexMap[t.X, t.Y].Type = NodeType.None;
            // var t = cube_to_axial(cube_round(axial_to_cube(new Hex((decimal)q, (decimal)r))));
        }

    }
}