﻿using MapGenerator.Map.PathFinding;
using MapGenerator.Rendering;
using MapGenerator.Types;

namespace MapGenerator.Map
{
    public class WorldController
    {
        public IndexSize Bounds => _worldMap.Bounds;
        private readonly WorldMap _worldMap;

        public WorldController(WorldMap worldMap)
        {
            _worldMap = worldMap;
        }

        public RouteInfo GetRouting()
        {
            var route = _worldMap.GetRoute();
            return new RouteInfo(route);
        }

        public void RenderTo(Renderer renderer)
        {
            _worldMap.DrawTo(renderer);
        }

        public void AddBuilding(Position mousePosition)
        {
            _worldMap.AddBuilding(mousePosition);
        }
    }
}