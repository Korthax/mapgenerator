﻿using System;
using System.Collections.Generic;
using System.Linq;
using MapGenerator.Map.Generation.Middleware;
using MapGenerator.Map.PathFinding;
using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation
{
    public class SpawnGenerator
    {
        private readonly IPathFinder _pathFinder;

        public SpawnGenerator(IPathFinder pathFinder)
        {
            _pathFinder = pathFinder;
        }

        public SpawnPoints For(IslandContext context, int clearance, int desiredClearance)
        {
            var island = context.GetLargestIsland();

            var spawns = new List<HexNode>
            {
                island.TopLeft(4),
                island.TopRight(4),
                island.BottomRight(4),
                island.BottomLeft(4)
            };

            while (clearance > 0)
            {
                var paths = new List<Route>();
                for(var i = 0; i < spawns.Count; i++)
                {
                    var routes = LinkSpawnPoints(context, spawns, spawns[i], clearance);

                    if(routes.Count == 0)
                    {
                        var nodeFinder = new NodeFinder(spawns[i], context.Map);

                        while (true)
                        {
                            var newSpawn = nodeFinder.Next();

                            if(newSpawn == null)
                                break;

                            routes = LinkSpawnPoints(context, spawns, newSpawn, clearance);
                            if (routes.Count <= 0)
                                continue;

                            spawns[i] = newSpawn;
                            break;
                        }
                    }

                    paths.AddRange(routes);
                }

                if(paths.Any(x => x.Count > 0))
                {
                    var longestPath = paths
                        .OrderByDescending(x => x.Count)
                        .First();

                    if (longestPath.Clearance < desiredClearance)
                    {
                        foreach (var node in longestPath)
                        {
                            if(node.Clearance >= desiredClearance)
                                continue;

                            foreach (var neighbour in node.GetNeighbours(desiredClearance, context.Map))
                            { 
                                if(neighbour.Type == NodeType.Forest)
                                    neighbour.Type = NodeType.Grassland;
                            }
                        }

                        new GenerateClearance().Process(new NullPipeline<IslandContext>(), context);
                    }

                    return SpawnPoints.Valid(longestPath.Start, longestPath.End);
                }

                clearance--;
            }

            throw new Exception();
        }

        private List<Route> LinkSpawnPoints(IslandContext context, List<HexNode> spawns, HexNode spawn, int clearance)
        {
            var routes = new List<Route>();
            foreach(var otherSpawn in spawns)
            {
                if(spawn.Equals(otherSpawn))
                    continue;

                var path = _pathFinder.Search(context.Map, spawn.HexPosition, otherSpawn.HexPosition, clearance);

                if(0 < path.Count)
                    routes.Add(path);
            }

            return routes;
        }
    }
}