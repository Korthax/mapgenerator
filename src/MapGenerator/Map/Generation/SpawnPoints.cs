﻿using MapGenerator.Map.PathFinding;
using MapGenerator.Rendering;
using MapGenerator.Types;
using Microsoft.Xna.Framework;

namespace MapGenerator.Map.Generation
{
    public class SpawnPoints
    {
        public Node Player { get; }
        public Node Enemy { get; }
        private readonly bool _isValid;

        public static SpawnPoints Valid(Node end, Node start)
        {
            return new SpawnPoints(end, start, true);
        }

        public static SpawnPoints Invalid(Node end, Node start)
        {
            return new SpawnPoints(end, start, false);
        }

        private SpawnPoints(Node player, Node enemy, bool isValid)
        {
            Player = player;
            Enemy = enemy;
            _isValid = isValid;
        }

        public void DrawTo(Renderer renderer, IndexSize tileSize)
        {
            if(Enemy != null)
                renderer.Render(new Position(Enemy.Hex.Position.X, Enemy.Hex.Position.Y), tileSize, _isValid ? Color.Orange : Color.PeachPuff);

            if(Player != null)
                renderer.Render(new Position(Player.Hex.Position.X, Player.Hex.Position.Y), tileSize, _isValid ? Color.Orange : Color.PeachPuff);
        }
    }
}