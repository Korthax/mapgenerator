﻿using System;
using System.Collections.Generic;
using MapGenerator.Map.Generation.Middleware;
using MapGenerator.Map.PathFinding;
using MapGenerator.Map.PathFinding.Heuristics;
using MapGenerator.Pipelines;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation
{
    public static class IslandGenerator
    {
        public static WorldMap Generate(IndexSize indexSize, IndexSize tileSize, RandomGenerator random)
        {
            var pipeline = new PipelineBuilder<IslandContext>()
                .AddToEnd(new InitialiseNodes())
                .AddToEnd(new GenerateNoise())
                .AddToEnd(new SmoothHeights())
                .AddToEnd(new AssignTypes())
                .AddToEnd(new SmoothCoastline())
                .AddToEnd(new CreateSpits())
                .AddToEnd(new CreateBays())
                .AddToEnd(new CreateMountainEdges())
                .AddToEnd(new CreateLakes())
                .AddToEnd(new GroupIslands())
                .AddToEnd(new GenerateIslandResourceAreas())
                .AddToEnd(new GenerateClearance())
                .Build();

            var hexMap = HexMap.Generate(indexSize.Width, indexSize.Height);
            var pipelineContext = new IslandContext(hexMap, indexSize, tileSize, random);
            pipeline.Next(pipelineContext);

            //var resources = PoissonDiskSampling.SampleRectangle(Position.Zero, indexSize * tileSize, 2 * 2);

            //resources.RemoveAll(x =>
            //{
            //    var index = (x / tileSize).ToIndex();
            //    return nodes[index.X, index.Y].Type != NodeType.Forest;
            //});

            var pathFinder = new AStarPathFinder(new GrassNeighbours(), new  ManhattanHeuristic());
            var spawnGenerator = new SpawnGenerator(pathFinder);
            var spawnPoints = spawnGenerator.For(pipelineContext, 1, 2);

            var startingPoints = new List<Position>();
            return new WorldMap(hexMap, indexSize, tileSize, startingPoints, spawnPoints, pathFinder);
        }
    }

    public class NodeFinder
    {
        private readonly HashSet<HexNode> _processed;
        private readonly Queue<HexNode> _fillList;
        private readonly HexMap _nodes;

        public NodeFinder(HexNode start, HexMap nodes)
        {
            _nodes = nodes;
            _processed = new HashSet<HexNode>();
            _fillList = new Queue<HexNode>();
            _fillList.Enqueue(start);
        }

        public HexNode Next()
        {
            while (_fillList.Count > 0)
            {
                var node = _fillList.Dequeue();

                if (!_processed.Contains(node))
                    _processed.Add(node);
                else
                    continue;

                if (node.IsBiomeOf(BiomeType.Grassland) && node.Clearance >= 3)
                    return node;

                foreach (var neighbourNode in _nodes.GetNeighbours(node, 1))
                {
                    var validNodeType = node.IsTraversable();
                    if (!validNodeType)
                        continue;

                    _fillList.Enqueue(neighbourNode);
                }
            }

            return null;
        }
    }
}