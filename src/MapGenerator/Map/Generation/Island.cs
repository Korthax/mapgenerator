﻿using System.Collections.Generic;
using System.Linq;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation
{
    public class Island
    {
        public int Count => _nodes.Count;
        public int Id { get; }

        private readonly HashSet<HexNode> _nodes;
        private readonly IndexSize _mapSize;

        public static Island From(HashSet<HexNode> nodes, IndexSize mapSize, int id)
        {
            var minDistance = float.MaxValue;
            var maxDistance = float.MaxValue;

            var topLeft = Position.Zero;
            var bottomRight = new Position(mapSize.Width, mapSize.Height);

            foreach(var node in nodes)
            {
                var min = node.DistanceFrom(topLeft);
                var max = node.DistanceFrom(bottomRight);

                if(min < minDistance)
                    minDistance = min;

                if (max < maxDistance)
                    maxDistance = max;
            }

            return new Island(nodes, id, mapSize);
        }

        private Island(HashSet<HexNode> nodes, int id, IndexSize mapSize)
        {
            _nodes = nodes;
            _mapSize = mapSize;
            Id = id;
        }

        public HexNode TopLeft(int clearance)
        {
            HexNode minNode = null;
            var minDistance = float.MaxValue;

            //var topLeft = Index.Zero;

            //foreach (var node in _nodes)
            //{
            //    if (node.Type != NodeType.Grassland || node.Clearance < clearance)
            //        continue;

            //    var min = Index.DistanceSquared(topLeft, node.Hex.Index);

            //    if (min < minDistance)
            //    {
            //        minDistance = min;
            //        minNode = node;
            //    }
            //}

            return minNode;
        }

        public HexNode BottomRight(int clearance)
        {
            HexNode maxNode = null;
            var maxDistance = float.MaxValue;

            //var bottomRight = new Index(_mapSize.Width, _mapSize.Height);

            //foreach (var node in _nodes)
            //{
            //    if (node.Type != NodeType.Grassland || node.Clearance < clearance)
            //        continue;

            //    var max = Index.DistanceSquared(bottomRight, node.Hex.Index);

            //    if (max < maxDistance)
            //    {
            //        maxDistance = max;
            //        maxNode = node;
            //    }
            //}

            return maxNode;
        }

        public HexNode BottomLeft(int clearance)
        {
            HexNode maxNode = null;
            var maxDistance = float.MaxValue;

            var bottomRight = new Index(0, _mapSize.Height);

            //foreach (var node in _nodes)
            //{
            //    if (node.Type != NodeType.Grassland || node.Clearance < clearance)
            //        continue;

            //    var max = Index.DistanceSquared(bottomRight, node.Hex.Index);

            //    if (max < maxDistance)
            //    {
            //        maxDistance = max;
            //        maxNode = node;
            //    }
            //}

            return maxNode;
        }

        public HexNode TopRight(int clearance)
        {
            HexNode maxNode = null;
            var maxDistance = float.MaxValue;

            var bottomRight = new Index(_mapSize.Width, 0);

            //foreach (var node in _nodes)
            //{
            //    if (node.Type != NodeType.Grassland || node.Clearance < clearance)
            //        continue;

            //    var max = Index.DistanceSquared(bottomRight, node.Hex.Index);

            //    if (max < maxDistance)
            //    {
            //        maxDistance = max;
            //        maxNode = node;
            //    }
            //}

            return maxNode;
        }

        public List<HexNode> ToList()
        {
            return _nodes.ToList();
        }
    }
}