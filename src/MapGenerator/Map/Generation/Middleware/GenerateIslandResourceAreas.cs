﻿using System;
using System.Collections.Generic;
using MapGenerator.Extensions;
using MapGenerator.Map.Generation.Noise;
using MapGenerator.Pipelines;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation.Middleware
{
    public class GenerateIslandResourceAreas : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            var largestIsland = context.GetLargestIsland().ToList();
            if(largestIsland == null)
            {
                pipeline.Next(context);
                return;
            }

            var numberOfTiles = (int)(largestIsland.Count / 100d * 25);
            var tenPercent = (int)(numberOfTiles * 0.1);
            var pointsRemaining = numberOfTiles;

            var forestTiles = new List<int>();
            while (pointsRemaining > 2)
            {
                var pointsToTake = context.Random.Next(7, Math.Max(tenPercent, 21));
                pointsRemaining -= pointsToTake;

                var size = (int)Math.Sqrt(pointsToTake);
                var noiseMap = NoiseGenerator.GenerateNoise(new IndexSize(size, size), new NoiseOptions { Seed = context.Random.Seed + 10 });

                int tileIndex;
                do
                {
                    tileIndex = context.Random.Next(0, largestIsland.Count);
                } while (forestTiles.Contains(tileIndex) || !largestIsland[tileIndex].IsBiomeOf(BiomeType.Grassland));

                forestTiles.Add(tileIndex);

                var centre = largestIsland[tileIndex];
                for (var y = 0; y < noiseMap.Height(); y++)
                {
                    for (var x = 0; x < noiseMap.Width(); x++)
                    {
                        if (noiseMap[x, y] <= 0)
                            continue;

                        var node = context.Map.Get(centre.Hex + OldHexPosition.FromOffset(new Index(x, y), context.TileSize));
                        if (node.Type != NodeType.Grassland)
                            continue;

                        node.Type = NodeType.Forest;
                        pointsToTake -= 1;
                    }
                }
            }

            pipeline.Next(context);
        }
    }
}