﻿using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class CreateSpits : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            foreach (var node in context.Map)
            {
                if (!node.IsTraversable())
                    continue;

                var coastCount = 0;
                foreach(var neighbourNode in context.Map.GetNeighbours(node, 1))
                {
                    if (neighbourNode.IsBiomeOf(BiomeType.Coast))
                        coastCount++;
                }

                if (coastCount < 6)
                    continue;

                node.SetBiomeType(BiomeType.Coast);
            }

            pipeline.Next(context);
        }
    }
}