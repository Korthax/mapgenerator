﻿using System.Linq;
using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class CreateMountainEdges : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            foreach (var node in context.Map)
            { 
                if (node.IsBiomeOf(BiomeType.Grassland))
                        continue;

                var isEdge = context.Map.GetNeighbours(node, 1)
                    .Any(neighbourNode => neighbourNode.IsBiomeOf(BiomeType.Mountain));

                if (isEdge)
                    node.SetBiomeType(BiomeType.Hill);
            }

            pipeline.Next(context);
        }
    }
}