﻿using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class InitialiseNodes : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            pipeline.Next(context);
        }
    }
}