﻿using MapGenerator.Map.Generation.Noise;
using MapGenerator.Pipelines;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation.Middleware
{
    public class GenerateNoise : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            var noise = NoiseGenerator.GenerateNoise(context.Size, new NoiseOptions { Seed = context.Random.Seed });

            for (var x = 0; x < noise.GetUpperBound(0) + 1; x++)
            {
                for (var y = 0; y < noise.GetUpperBound(1) + 1; y++)
                {
                    context.Map.SetElevation(HexPosition.From(x, y), (int)noise[x,y]);
                }
            }

            pipeline.Next(context);
        }
    }
}