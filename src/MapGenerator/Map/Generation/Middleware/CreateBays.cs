﻿using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class CreateBays : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            var continueProcessing = true;
            while (continueProcessing)
                continueProcessing = ProcessNodes(context);

            pipeline.Next(context);
        }

        private static bool ProcessNodes(IslandContext context)
        {
            var nodeProcessed = false;
            foreach (var node in context.Map)
            {
                if (node.IsBiomeOf(BiomeType.Coast))
                    continue;

                for (var i = 0; i < Node.NumberOfNeighbours / 2; i++)
                {
                    var neighbour = context.Map.GetNeighbour(node, i);
                    var opposite = context.Map.GetNeighbour(node, i + 3);

                    if (!neighbour.IsBiomeOf(BiomeType.Ocean) || !opposite.IsBiomeOf(BiomeType.Ocean))
                        continue;

                    node.SetBiomeType(neighbour.GetBiomeType());
                    nodeProcessed = true;
                }
            }

            return nodeProcessed;
        }
    }
}