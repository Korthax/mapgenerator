﻿using System.Collections.Generic;
using System.Linq;
using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class CreateLakes : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            var waterNodes = new HashSet<HexNode>();
            foreach (var node in context.Map)
            {
                if (!node.IsBiomeOf(BiomeType.Ocean))
                    continue;

                waterNodes.Add(node);
            }

            var processed = new HashSet<HexNode>();
            while (waterNodes.Count > 0)
            {
                var currentNode = waterNodes.First();
                processed.Add(currentNode);

                var fillList = new Queue<HexNode>();
                fillList.Enqueue(currentNode);
                var waterMass = new HashSet<HexNode> { currentNode };

                while (fillList.Count > 0)
                {
                    var n = fillList.Dequeue();

                    if (waterNodes.Contains(n))
                        waterNodes.Remove(n);
                    else
                        continue;

                    foreach (var neighbourNode in context.Map.GetNeighbours(n, 1))
                    {
                        if (!neighbourNode.IsBiomeOf(BiomeType.Ocean))
                            continue;

                        waterMass.Add(neighbourNode);
                        fillList.Enqueue(neighbourNode);
                    }
                }


                var nodeType = BiomeType.Lake;
                foreach (var node in waterMass)
                {
                    if (context.Map.IsEdgeNode(node))
                        continue;

                    nodeType = BiomeType.Ocean;
                }

                foreach (var node in waterMass)
                    node.SetBiomeType(nodeType);
            }

            pipeline.Next(context);
        }
    }
}