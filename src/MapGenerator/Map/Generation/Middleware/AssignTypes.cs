﻿using System;
using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class AssignTypes : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            foreach (var node in context.Map)
            {
                if (Math.Abs(node.Elevation) < float.Epsilon)
                {
                    node.SetBiomeType(BiomeType.Ocean);
                    node.Elevation = 0;
                }
                else
                {
                    var bordersOcean = false;
                    foreach (var neighbour in context.Map.GetNeighbours(node, 1))
                    {
                        if (Math.Abs(neighbour.Elevation) < float.Epsilon)
                            bordersOcean = true;
                    }

                    if (bordersOcean)
                        node.SetBiomeType(BiomeType.Coast);
                    else if (node.Elevation > 10)
                        node.SetBiomeType(BiomeType.Mountain);
                    else
                        node.SetBiomeType(BiomeType.Grassland);
                }
            }

            pipeline.Next(context);
        }
    }
}