﻿using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class GenerateClearance : IMiddleware<IslandContext>
    {
        private const int MaxUnitSize = 10;

        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            foreach (var node in context.Map)
            {
                var currentSize = 0;
                var shouldExpand = true;

                if(node.IsBiomeOf(BiomeType.Grassland))
                    continue;

                do
                {
                    currentSize++;
                    foreach (var neighbourNode in context.Map.GetNeighbours(node, currentSize))
                    {
                        if(neighbourNode.IsBiomeOf(BiomeType.Grassland))
                            shouldExpand = false;
                    }

                } while(currentSize <= MaxUnitSize && shouldExpand);

                node.SetClearance(currentSize);
            }

            pipeline.Next(context);
        }
    }
}