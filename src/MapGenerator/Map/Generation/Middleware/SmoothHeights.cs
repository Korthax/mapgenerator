﻿using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class SmoothHeights : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            foreach (var currentNode in context.Map)
            {
                var heightSum = 0f;
                var sameHeight = false;
                foreach (var neighbour in context.Map.GetNeighbours(currentNode, 1))
                {
                    var nextHeight = neighbour.Elevation;
                    sameHeight = sameHeight || nextHeight == currentNode.Elevation;
                    heightSum += nextHeight;
                }

                if (!sameHeight)
                    currentNode.Elevation = heightSum / 6;
            }

            pipeline.Next(context);
        }
    }
}