﻿using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class SmoothCoastline : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            foreach (var node in context.Map)
            {
                if (node.IsBiomeOf(BiomeType.Coast))
                    continue;

                if (!TrySmooth(context, node))
                    continue;

                foreach (var neighbour in context.Map.GetNeighbours(node, 1))
                    TrySmooth(context, neighbour);

            }

            pipeline.Next(context);
        }

        private static bool TrySmooth(IslandContext context, HexNode node)
        {
            var oceanCount = 0;
            foreach(var neighbourNode in context.Map.GetNeighbours(node, 1))
            {
                if(neighbourNode.IsBiomeOf(BiomeType.Ocean))
                    oceanCount++;
            }

            if(oceanCount < 4)
                return false;

            node.SetBiomeType(BiomeType.Ocean);
            node.Elevation = 0;
            return true;
        }
    }
}