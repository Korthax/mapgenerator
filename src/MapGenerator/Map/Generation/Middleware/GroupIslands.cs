﻿using System.Collections.Generic;
using System.Linq;
using MapGenerator.Pipelines;

namespace MapGenerator.Map.Generation.Middleware
{
    public class GroupIslands : IMiddleware<IslandContext>
    {
        public void Process(IPipeline<IslandContext> pipeline, IslandContext context)
        {
            var land = new HashSet<HexNode>();

            foreach (var node in context.Map)
            {
                if (!node.IsTraversable())
                    continue;

                land.Add(node);
            }

            var processed = new HashSet<HexNode>();
            while (land.Count > 0)
            {
                var currentNode = land.First();
                processed.Add(currentNode);

                var islandId = context.Islands.Count;

                var islandNodes = new HashSet<HexNode> { currentNode };

                var fillList = new Queue<HexNode>();
                fillList.Enqueue(currentNode);

                while (fillList.Count > 0)
                {
                    var n = fillList.Dequeue();

                    if (land.Contains(n))
                        land.Remove(n);
                    else
                        continue;

                    foreach (var neighbourNode in context.Map.GetNeighbours(n, 1))
                    {
                        if (!neighbourNode.IsTraversable())
                            continue;

                        islandNodes.Add(neighbourNode);
                        fillList.Enqueue(neighbourNode);
                    }
                }

                context.AddIsland(Island.From(islandNodes, context.Size, islandId));
            }

            pipeline.Next(context);
        }
    }
}