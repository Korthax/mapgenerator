﻿using System;
using System.Collections.Generic;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation
{
    public class PoissonDiskSampling
    {
        public const float Pi = (float)Math.PI;
        public const float HalfPi = (float)(Math.PI / 2);
        public const float TwoPi = (float)(Math.PI * 2);
        public const int DefaultPointsPerIteration = 30;

        public static readonly Random Random = new Random();
        private static readonly float SquareRootTwo = (float)Math.Sqrt(2);

        private struct Settings
        {
            public Position TopLeft, LowerRight;
            public Position Dimensions;
            public float MinimumDistance;
            public float CellSize;
            public int GridWidth, GridHeight;
        }

        private struct State
        {
            public Position[,] Grid;
            public List<Position> ActivePoints, Points;
        }

        public static List<Position> SampleRectangle(Position topLeft, Size size, float minimumDistance)
        {
            return Sample(topLeft, size, minimumDistance, DefaultPointsPerIteration);
        }

        private static List<Position> Sample(Position topLeft, Size size, float minimumDistance, int pointsPerIteration)
        {
            var lowerRight = topLeft + size;

            var settings = new Settings
            {
                TopLeft = topLeft,
                LowerRight = lowerRight,
                Dimensions = lowerRight - topLeft,
                CellSize = minimumDistance / SquareRootTwo,
                MinimumDistance = minimumDistance
            };

            settings.GridWidth = (int)(settings.Dimensions.X / settings.CellSize) + 1;
            settings.GridHeight = (int)(settings.Dimensions.Y / settings.CellSize) + 1;

            var state = new State
            {
                Grid = new Position[settings.GridWidth, settings.GridHeight],
                ActivePoints = new List<Position>(),
                Points = new List<Position>()
            };

            AddFirstPoint(ref settings, ref state);

            while (state.ActivePoints.Count != 0)
            {
                var listIndex = Random.Next(state.ActivePoints.Count);

                var point = state.ActivePoints[listIndex];
                var found = false;

                for (var k = 0; k < pointsPerIteration; k++)
                    found |= AddNextPoint(point, ref settings, ref state);

                if (!found)
                    state.ActivePoints.RemoveAt(listIndex);
            }

            return state.Points;
        }

        private static void AddFirstPoint(ref Settings settings, ref State state)
        {
            var randomDouble = Random.NextDouble();
            var xr = settings.TopLeft.X + settings.Dimensions.X * randomDouble;

            randomDouble = Random.NextDouble();
            var yr = settings.TopLeft.Y + settings.Dimensions.Y * randomDouble;

            var point = new Position((float)xr, (float)yr);

            var index = Denormalize(point, settings.TopLeft, settings.CellSize);

            state.Grid[(int)index.X, (int)index.Y] = point;

            state.ActivePoints.Add(point);
            state.Points.Add(point);
        }

        private static bool AddNextPoint(Position point, ref Settings settings, ref State state)
        {
            var q = GenerateRandomAround(point, settings.MinimumDistance);

            if(q.X < settings.TopLeft.X || q.X >= settings.LowerRight.X || q.Y < settings.TopLeft.Y || q.Y >= settings.LowerRight.Y)
                return false;

            var qIndex = Denormalize(q, settings.TopLeft, settings.CellSize);
            var tooClose = false;

            for (var i = (int)Math.Max(0, qIndex.X - 2); i < Math.Min(settings.GridWidth, qIndex.X + 3) && !tooClose; i++)
            for (var j = (int)Math.Max(0, qIndex.Y - 2); j < Math.Min(settings.GridHeight, qIndex.Y + 3) && !tooClose; j++)
                if (state.Grid[i, j] != null && Position.Distance(state.Grid[i, j], q) < settings.MinimumDistance)
                    tooClose = true;

            if(tooClose)
                return false;

            state.ActivePoints.Add(q);
            state.Points.Add(q);
            state.Grid[(int)qIndex.X, (int)qIndex.Y] = q;
            return true;
        }

        private static Position GenerateRandomAround(Position center, float minimumDistance)
        {
            var d = Random.NextDouble();
            var radius = minimumDistance + minimumDistance * d;

            d = Random.NextDouble();
            var angle = TwoPi * d;

            var newX = radius * Math.Sin(angle);
            var newY = radius * Math.Cos(angle);

            return new Position((float)(center.X + newX), (float)(center.Y + newY));
        }

        private static Position Denormalize(Position point, Position origin, double cellSize)
        {
            return new Position((int)((point.X - origin.X) / cellSize), (int)((point.Y - origin.Y) / cellSize));
        }
    }
}