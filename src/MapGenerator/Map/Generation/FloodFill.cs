﻿using System;
using System.Collections.Generic;

namespace MapGenerator.Map.Generation
{
    public static class FloodFill
    {
        public static HashSet<T> Test<T>(T start, Func<T, IEnumerable<T>> getNeighbours, Func<T, bool> shouldStop)
        {
            var result = new HashSet<T> { start };

            var fillList = new Queue<T>();
            fillList.Enqueue(start);

            var processed = new HashSet<T>();
            while (fillList.Count > 0)
            {
                var n = fillList.Dequeue();

                if(!processed.Contains(n) && !shouldStop(n))
                    processed.Add(n);
                else
                    continue;

                foreach(var neighbour in getNeighbours(n))
                {
                    if(shouldStop(neighbour))
                        continue;

                    result.Add(neighbour);
                    fillList.Enqueue(neighbour);
                }
            }

            return result;
        }
    }
}