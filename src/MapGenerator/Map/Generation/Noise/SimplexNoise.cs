﻿using System;
using System.Linq;

namespace MapGenerator.Map.Generation.Noise
{
    public class SimplexNoise
    {
        private readonly double F2 = 0.5 * (Math.Sqrt(3.0) - 1.0);
        private readonly double G2 = (3.0 - Math.Sqrt(3.0)) / 6.0;

        private readonly float[] _gradients =
        {
            1, 1, 0,
            -1, 1, 0,
            1, -1, 0,

            -1, -1, 0,
            1, 0, 1,
            -1, 0, 1,

            1, 0, -1,
            -1, 0, -1,
            0, 1, 1,

            0, -1, 1,
            0, 1, -1,
            0, -1, -1
        };

        private readonly int[] _permutationMod12;
        private readonly int[] _permutations;

        public SimplexNoise(Random random)
        {
            _permutations = new int[512];
            _permutationMod12 = new int[512];

            var permutationTable = BuildPermutationTable(random);
            for (var i = 0; i < 512; i++)
            {
                _permutations[i] = permutationTable[i & 255];
                _permutationMod12[i] = _permutations[i] % 12;
            }
        }

        public double Noise2D(double xin, double yin)
        {
            double n0 = 0; // Noise contributions from the three corners
            double n1 = 0;
            double n2 = 0;

            // Skew the input space to determine which simplex cell we're in
            var s = (xin + yin) * F2; // Hairy factor for 2D
            var i = (int)Math.Floor(xin + s);
            var j = (int)Math.Floor(yin + s);
            var t = (i + j) * G2;
            var x0 = xin - (i - t); // // Unskew the cell origin back to the x,y distances from the cell origin
            var y0 = yin - (j - t);

            // Determine which simplex we are in.
            int i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if (x0 > y0) // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            {
                i1 = 1;
                j1 = 0;
            }
            else // upper triangle, YX order: (0,0)->(0,1)->(1,1)
            {
                i1 = 0;
                j1 = 1;
            }

            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6
            var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            var y1 = y0 - j1 + G2;
            var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
            var y2 = y0 - 1.0 + 2.0 * G2;

            // Work out the hashed gradient indices of the three simplex corners
            var ii = i & 255;
            var jj = j & 255;

            // Calculate the contribution from the three corners
            var t0 = 0.5 - x0 * x0 - y0 * y0;
            if (t0 >= 0)
            {
                var gi0 = _permutationMod12[ii + _permutations[jj]] * 3;
                t0 *= t0;
                n0 = t0 * t0 * (_gradients[gi0] * x0 + _gradients[gi0 + 1] * y0); // (x,y) of grad3 used for 2D gradient
            }

            var t1 = 0.5 - x1 * x1 - y1 * y1;
            if (t1 >= 0)
            {
                var gi1 = _permutationMod12[ii + i1 + _permutations[jj + j1]] * 3;
                t1 *= t1;
                n1 = t1 * t1 * (_gradients[gi1] * x1 + _gradients[gi1 + 1] * y1);
            }

            var t2 = 0.5 - x2 * x2 - y2 * y2;
            if (t2 >= 0)
            {
                var gi2 = _permutationMod12[ii + 1 + _permutations[jj + 1]] * 3;
                t2 *= t2;
                n2 = t2 * t2 * (_gradients[gi2] * x2 + _gradients[gi2 + 1] * y2);
            }

            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 70.0 * (n0 + n1 + n2);
        }

        private static int[] BuildPermutationTable(Random random)
        {
            var p = new int[256];
            for (var i = 0; i < 256; i++)
                p[i] = i;

            return p
                .OrderBy(r => random.Next())
                .ToArray();
        }
    }
}