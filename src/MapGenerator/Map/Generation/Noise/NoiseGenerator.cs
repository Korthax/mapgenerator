﻿using System;
using System.Collections.Generic;
using System.Linq;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation.Noise
{
    public static class NoiseGenerator
    {
        public static double[,] GenerateNoise(IndexSize size, NoiseOptions options)
        {
            var elevations = MixNoise(size, new[] { 1, 1 / 4d, 1 / 8d, 1 / 16d }, 2, options.Seed);
            for (var y = 0; y < size.Height; y++)
            {
                for (var x = 0; x < size.Width; x++)
                {
                    var nx = x / (double)size.Width - 0.5;
                    var ny = y / (double)size.Height - 0.5;
                    var d = 2 * Math.Sqrt(nx * nx + ny * ny);

                    var baseElevation = elevations[x, y];
                    var elevation = Math.Round(baseElevation * options.Tiers) / options.Tiers;
                    elevation = (elevation + options.A) * (1 - options.B * Math.Pow(d, options.C));

                    if (elevation < 0.0)
                        elevation = 0.0;

                    elevation = (int)Math.Round(((int)(options.MaxHeight * elevation) | 0) / options.Increments) * options.Increments;

                    if (elevation < 10.0)
                        elevation = 0.0;

                    elevations[x, y] = (int)elevation;
                }
            }

            return elevations;
        }

        private static double[,] MixNoise(IndexSize size, double[] spectrum, double frequency, int seed)
        {
            var maps = new List<double[,]>();
            var amplitudes = new List<double>();

            var scale = 0.0;
            var exponent = 1;
            for (var octave = 0; octave < spectrum.Length; octave++, exponent *= 2)
            {
                scale += spectrum[octave];
                maps.Add(FillNoise(size, frequency * exponent, seed + octave));
                amplitudes.Add(spectrum[octave]);
            }

            return AddMaps(size, maps, amplitudes.Select(a => a / scale).ToList());
        }

        private static double[,] AddMaps(IndexSize size, List<double[,]> maps, List<double> amplitudes)
        {
            var mapsCount = maps.Count;
            if (mapsCount != amplitudes.Count)
                throw new Exception("maps and amplitudes must be same length");

            var output = new double[size.Width, size.Height];
            for (var y = 0; y < size.Height; y++)
            {
                for (var x = 0; x < size.Width; x++)
                {
                    var z = 0d;
                    for (var i = 0; i < mapsCount; i++)
                        z += amplitudes[i] * maps[i][x, y];

                    output[x, y] = z;
                }
            }

            return output;
        }

        private static double[,] FillNoise(IndexSize size, double frequency, int seed)
        {
            var map = new double[size.Width, size.Height];
            var noise = new SimplexNoise(new Random(seed));
            var aspect = size.Width / size.Height;
            for (var y = 0; y < size.Height; y++)
            {
                for (var x = 0; x < size.Width; x++)
                {
                    var nx = x / (float)size.Width - 0.5f;
                    var ny = y / (float)size.Height - 0.5f;
                    var z = noise.Noise2D(nx * frequency * aspect, ny * frequency);
                    map[x, y] = z / 2 + 0.5;
                }
            }

            return map;
        }
    }
}