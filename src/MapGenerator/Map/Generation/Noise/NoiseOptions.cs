﻿namespace MapGenerator.Map.Generation.Noise
{
    public class NoiseOptions
    {
        public double Increments { get; set; } = 10d;
        public int Tiers { get; set; } = 4;
        public int MaxHeight { get; set; } = 20;
        public double A { get; set; } = 0.20;
        public double B { get; set; } = 1.00;
        public double C { get; set; } = 2.50;
        public int Seed { get; set; }
    }
}