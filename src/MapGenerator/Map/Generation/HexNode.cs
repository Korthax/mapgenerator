﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MapGenerator.Rendering;
using MapGenerator.Types;
using Microsoft.Xna.Framework;

namespace MapGenerator.Map.Generation
{
    public class HexNode
    {
        public bool Traversable => Layers.IsTraversable();
        public HexPosition HexPosition { get; }
        public HexLayers Layers { get; }
        public float Elevation { get; set; }
        public int Clearance { get; set; }
        public Position Position { get; }

        private readonly HexagonShape _hexagonShape;

        public static HexNode From(int x, int y, int z, HexagonShape hexagonShape)
        {
            var position = Position.From(x, y, z, hexagonShape);
            return new HexNode(new HexPosition(x, y, z), position, new HexLayers(), hexagonShape);
        }

        private HexNode(HexPosition hexPosition, Position position, HexLayers hexLayers, HexagonShape hexagonShape)
        {
            HexPosition = hexPosition;
            Layers = hexLayers;
            Position = position;
            _hexagonShape = hexagonShape;
        }

        public bool IsBiomeOf(BiomeType biomeType)
        {
            return Layers.IsBiomeOf(biomeType);
        }

        public void SetBiomeType(BiomeType biomeType)
        {
            Layers.Add(biomeType);
        }

        public void SetClearance(int clearance)
        {
            Clearance = clearance;
        }

        public void RenderTo(Renderer renderer)
        {
            renderer.Render(Position, _hexagonShape.Origin, _hexagonShape.Size, Color.White);
        }

        protected bool Equals(HexNode other)
        {
            return Equals(HexPosition, other.HexPosition);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((HexNode)obj);
        }

        public override int GetHashCode()
        {
            return HexPosition != null ? HexPosition.GetHashCode() : 0;
        }

        public BiomeType GetBiomeType()
        {
            return Layers.GetBiomeType();
        }

        public bool IsTraversable()
        {
            return Layers.IsTraversable();
        }

        public float DistanceFrom(Position position)
        {
            throw new NotImplementedException();
        }

        public float DistanceFrom(HexNode position)
        {
            throw new NotImplementedException();
        }

        public Position ToPosition()
        {
            return Position.From(HexPosition.X, HexPosition.Y, HexPosition.Z, _hexagonShape);
        }

        public double CostFrom(HexNode start)
        {
            if (start.Clearance < Clearance)
                return 1;

            return Clearance == start.Clearance ? 5 : 8;
        }
    }

    public class HashMap<T> :  IEnumerable<T>
    {
        private readonly Dictionary<int, T> _values;

        public HashMap() : this(new List<T>())
        {
        }

        public void Add(T item)
        {
            _values[item.GetHashCode()] = item;
        }

        public bool Contains(T item)
        {
            return _values.ContainsKey(item.GetHashCode());
        }

        public T Get(object item)
        {
            return _values[item.GetHashCode()];
        }

        public HashMap(IEnumerable<T> values)
        {
            _values = values.ToDictionary(x => x.GetHashCode(), x => x);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _values.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class HexLayers
    {
        private readonly Stack<HexLayer> _layers;

        public HexLayers()
        {
            _layers = new Stack<HexLayer>(new List<HexLayer> { new HexLayer(BiomeType.Ocean) });
        }

        public bool IsTraversable()
        {
            return _layers.Peek().IsTraversable();
        }

        public void Add(BiomeType biomeType)
        {
            
        }

        public bool IsBiomeOf(BiomeType biomeType)
        {
            return _layers.Peek().IsBiomeOf(biomeType);
        }

        public BiomeType GetBiomeType()
        {
            return _layers.Pop().Biome;
        }
    }

    public class HexLayer
    {
        public BiomeType Biome { get; }

        public HexLayer(BiomeType biome)
        {
            Biome = biome;
        }

        public bool IsTraversable()
        {
            return Biome.IsTraversable();
        }

        public bool IsBiomeOf(BiomeType biomeType)
        {
            return Biome == biomeType;
        }
    }

    public static class BiomeExtensions
    {
        public static bool IsTraversable(this BiomeType biomeType)
        {
            return biomeType == BiomeType.Grassland;
        }
    }

    public enum BiomeType
    {
        Grassland,
        Ocean,
        Hill,
        Mountain,
        Coast,
        Lake
    }

    public class HexPosition
    {
        public int X { get; }
        public int Y { get; }
        public int Z { get; }

        public static HexPosition RoundFrom(float q, float r, float s)
        {
            var x = (int)Math.Round(q);
            var y = (int)Math.Round(r);
            var z = (int)Math.Round(s);

            var xDiff = Math.Abs(x - q);
            var yDiff = Math.Abs(y - r);
            var sDiff = Math.Abs(z - s);

            if (xDiff > yDiff && xDiff > sDiff)
                x = -y - z;
            else if (yDiff > sDiff)
                y = -x - z;
            else
                z = -x - y;

            return new HexPosition(x, y, z);
        }

        public static HexPosition From(int col, int row)
        {
            var x = col - (row + (row & 1)) / 2;
            var z = row;
            var y = -x - z;

            return new HexPosition(x, y, z);
        }

        public HexPosition(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        protected bool Equals(HexPosition other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((HexPosition)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Z;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"{X}, {Y}, {Z}";
        }
    }

    public class HexMap : IEnumerable<HexNode>
    {
        private readonly HashMap<HexNode> _nodes;
        private readonly HexagonShape _shape;
        private HexPosition _highlight;

        public static HexMap Generate(int width, int height)
        {
            var layout = new HexagonShape(new Size(32, 32));

            var nodes = new HashMap<HexNode>();
            for (var r = 0; r < height; r++)
            {
                var rOffset = (int)Math.Floor(r / 2d);
                for (var q = -rOffset; q < width - rOffset; q++)
                    nodes.Add(HexNode.From(q, -q - r, r, layout));
            }

            return new HexMap(nodes, layout);
        }

        private HexMap(HashMap<HexNode> nodes, HexagonShape shape)
        {
            _nodes = nodes;
            _shape = shape;
        }

        public void Process(Action<HexNode> action)
        {
            foreach (var node in _nodes)
                action(node);
        }

        public void Pick(Position position)
        {
            var m = _shape.Orientation;
            var pt = new Position(position.X / _shape.Spacing.X, position.Y / _shape.Spacing.Y);
            var q = m.B0 * pt.X + m.B1 * pt.Y;
            var r = m.B2 * pt.X + m.B3 * pt.Y;

            _highlight = HexPosition.RoundFrom((float)q, (float)(-q - r), (float)r);
        }

        public IEnumerable<HexNode> GetNeighbours(HexNode node, int radius)
        {

        }

        public bool IsEdgeNode(HexNode node)
        {
            throw new NotImplementedException();
        }

        public HexNode GetNeighbour(HexNode node, int direction)
        {
            throw new NotImplementedException();
        }

        public void SetElevation(HexPosition hexPosition, int elevation)
        {
        }

        public HexNode Get(HexPosition startIndex)
        {
            throw new NotImplementedException();
        }

        public void DrawTo(Renderer renderer)
        {
            foreach (var node in _nodes)
               node.RenderTo(renderer);

            if(_highlight != null)
                renderer.RenderOutline(Position.From(_highlight.X, _highlight.Y, _highlight.Z, _shape), _shape.Origin, _shape.Size, Color.Orange);
        }

        public IEnumerator<HexNode> GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
	
    public struct Orientation
    {
        public double StartAngle { get; }
        public double F0 { get; }
        public double F1 { get; }
        public double F2 { get; }
        public double F3 { get; }
        public double B0 { get; }
        public double B1 { get; }
        public double B2 { get; }
        public double B3 { get; }

        public Orientation(double f0, double f1, double f2, double f3, double b0, double b1, double b2, double b3, double startAngle)
        {
            F0 = f0;
            F1 = f1;
            F2 = f2;
            F3 = f3;
            B0 = b0;
            B1 = b1;
            B2 = b2;
            B3 = b3;
            StartAngle = startAngle;
        }
    }

    public class HexagonShape
    {
        public Orientation Orientation { get; }
        public Size Size { get; }
        public Position Origin { get; }
        public Position Spacing { get; }

        public HexagonShape(Size size)
        {
            Origin = new Position(size.Width / 2, size.Height / 2);
            Spacing = new Position((float)(Math.Sqrt(3) / 3f) * size.Width, size.Height * 0.5f);
            Size = size;
            Orientation = new Orientation(Math.Sqrt(3.0), Math.Sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, Math.Sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5);
        }
    }

    public static class FloatExtensions
    {
        public static int IsEven(this float value)
        {
            return Math.Abs(value % 2) < float.Epsilon ? 1 : 0;
        }
    }
}