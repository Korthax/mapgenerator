﻿using System.Collections.Generic;
using System.Linq;
using MapGenerator.Types;

namespace MapGenerator.Map.Generation
{
    public class IslandContext
    {
        public RandomGenerator Random { get; }
        public IndexSize Size { get; }
        public HexMap Map { get; }
        public IndexSize TileSize { get; set; }
        public List<Island> Islands { get; }

        public IslandContext(HexMap map, IndexSize size, IndexSize tileSize, RandomGenerator random)
        {
            Map = map;
            Size = size;
            TileSize = tileSize;
            Random = random;
            Islands = new List<Island>();
        }

        public void AddIsland(Island islandNodes)
        {
            Islands.Add(islandNodes);
        }

        public Island GetLargestIsland()
        {
            return Islands.OrderByDescending(x => x.Count).FirstOrDefault();
        }
    }
}