﻿namespace MapGenerator.Map.PathFinding
{
    public class RouteInfo
    {
        private readonly Route _route;

        public RouteInfo(Route route)
        {
            _route = route;
        }

        public Path BuildPath()
        {
            return _route.ToPath();
        }
    }
}