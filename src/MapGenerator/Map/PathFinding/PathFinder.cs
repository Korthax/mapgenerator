﻿using System;
using System.Collections.Generic;
using System.Linq;
using MapGenerator.Map.Generation;
using MapGenerator.Types;

namespace MapGenerator.Map.PathFinding
{
    public interface IPathFinder
    {
        Route Search(HexMap graph, HexPosition startIndex, HexPosition endIndex, int clearance);
    }

    public class PathFinder : IPathFinder
    {
        private readonly INeighbourFunction _neighbourFunction;
        private readonly IHeuristic _heuristic;

        public PathFinder(IHeuristic heuristic, INeighbourFunction neighbourFunction)
        {
            _heuristic = heuristic;
            _neighbourFunction = neighbourFunction;
        }

        public Route Search(HexMap graph, HexPosition startIndex, HexPosition endIndex, int clearance)
        {
            if(clearance < 1)
                throw new ArgumentException("Clearance cannot be lower than 1.", nameof(clearance));

            var start = graph.Get(startIndex);
            var end = graph.Get(endIndex);

            var path = new Dictionary<HexNode, HexNode>();
            var costs = new Dictionary<HexNode, int>();

            var openList = new PriorityQueue<HexNode>();
            openList.Enqueue(start, 0);

            path.Add(start, null);
            costs.Add(start, 0);

            while (openList.Count > 0)
            {
                var current = openList.Dequeue();

                if (current.Equals(end))
                {
                    var result = new Route(new List<HexNode>());
                    while (current != null)
                    {
                        result.Push(current);
                        path.TryGetValue(current, out current);
                    }

                    return result;
                }

                foreach (var next in _neighbourFunction.GetNeighbours(graph, current, clearance))
                {
                    var newCost = costs[current];
                    if (costs.ContainsKey(next) && newCost >= costs[next])
                        continue;

                    costs[next] = newCost;
                    openList.Enqueue(next, newCost + _heuristic.CostFor(current, next, start, end));
                    path[next] = current;
                }
            }

            return new Route(new List<HexNode>());
        }
    }

    public interface INeighbourFunction
    {
        List<HexNode> GetNeighbours(HexMap nodes, HexNode current, int clearance);
    }

    public class GrassNeighbours : INeighbourFunction
    {
        public List<HexNode> GetNeighbours(HexMap nodes, HexNode current, int clearance)
        {
            return nodes
                .GetNeighbours(current, 1)
                .Where(x => x.IsBiomeOf(BiomeType.Grassland))
                .Where(x => clearance <= x.Clearance)
                .ToList();
        }
    }


    public interface IHeuristic
    {
        int CostFor(HexNode current, HexNode next, HexNode start, HexNode end);
    }
}