﻿using System.Collections.Generic;
using MapGenerator.Map.Generation;

namespace MapGenerator.Map.PathFinding
{
    public class AStarPathFinder : IPathFinder
    {
        private readonly INeighbourFunction _neighbourFunction;
        private readonly IHeuristic _heuristic;

        public AStarPathFinder(INeighbourFunction neighbourFunction, IHeuristic heuristic)
        {
            _neighbourFunction = neighbourFunction;
            _heuristic = heuristic;
        }

        public Route Search(HexMap graph, HexPosition startIndex, HexPosition endIndex, int clearance)
        {
            var cameFrom = new Dictionary<HexNode, HexNode>();
            var costSoFar = new Dictionary<HexNode, double>();

            var start = graph.Get(startIndex);
            var end = graph.Get(endIndex);

            var openList = new PriorityQueue<HexNode>();
            openList.Enqueue(start, 0);

            cameFrom[start] = start;
            costSoFar[start] = 0;

            while (openList.Count > 0)
            {
                var currentNode = openList.Dequeue();

                if (currentNode.Equals(end))
                    break;

                foreach (var next in _neighbourFunction.GetNeighbours(graph, currentNode, clearance))
                {
                    var newCost = costSoFar[currentNode] + next.CostFrom(currentNode);
                    if (costSoFar.ContainsKey(next) && newCost >= costSoFar[next])
                        continue;

                    costSoFar[next] = newCost;
                    var priority = newCost + _heuristic.CostFor(currentNode, next, start, end);
                    openList.Enqueue(next, priority);
                    cameFrom[next] = currentNode;
                }
            }

            return !cameFrom.ContainsKey(end) 
                ? new Route(new List<HexNode>()) 
                : Retrace(start, end, cameFrom);
        }

        private static Route Retrace(HexNode start, HexNode goal, Dictionary<HexNode, HexNode> cameFrom)
        {
            var route = new Route(new List<HexNode>());

            var current = goal;
            while (!cameFrom[current].Equals(start))
            {
                route.Push(current);
                current = cameFrom[current];
            }

            route.Push(current);
            return route;
        }
    }
}