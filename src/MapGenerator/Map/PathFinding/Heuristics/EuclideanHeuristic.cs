﻿using MapGenerator.Map.Generation;

namespace MapGenerator.Map.PathFinding.Heuristics
{
    public class EuclideanHeuristic : IHeuristic
    {
        public int CostFor(HexNode current, HexNode next, HexNode start, HexNode end)
        {
            return (int)next.DistanceFrom(end);
        }
    }
}