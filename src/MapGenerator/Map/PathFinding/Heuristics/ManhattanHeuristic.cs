﻿using System;
using MapGenerator.Map.Generation;
using MapGenerator.Types;

namespace MapGenerator.Map.PathFinding.Heuristics
{
    public class ManhattanHeuristic : IHeuristic
    {
        public int CostFor(HexNode current, HexNode next, HexNode start, HexNode end)
        {
            var cost = Math.Abs(next.Position.X - end.Position.X) + Math.Abs(next.Position.Y - end.Position.Y);
            var totalCost = new Position(start.Position.X - end.Position.X, start.Position.Y - end.Position.Y);
            var currentCost = new Position(next.Position.X - end.Position.X, start.Position.Y - end.Position.Y);
            var crossProduct = Math.Abs(currentCost.X * totalCost.Y - totalCost.X * currentCost.Y);
            return (int)(crossProduct * 0.001 + cost);
        }
    }
}