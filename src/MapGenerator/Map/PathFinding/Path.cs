﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MapGenerator.Map.Generation;
using MapGenerator.Types;

namespace MapGenerator.Map.PathFinding
{
    public class Path : IEnumerable<Position>
    {
        public int Count => _positions.Count;
        private readonly Queue<Position> _positions;

        public Path(Queue<Position> positions)
        {
            _positions = positions;
        }

        public Position Next()
        {
            return _positions.Dequeue();
        }

        public IEnumerator<Position> GetEnumerator()
        {
            return _positions.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class Route : IEnumerable<HexNode>
    {
        public int Count => _nodes.Count;
        public HexNode Start => _nodes.First();
        public HexNode End => _nodes.Last();
        public int Clearance => _nodes.Min(x => x.Clearance);

        private readonly List<HexNode> _nodes;

        public Route(List<HexNode> nodes)
        {
            _nodes = nodes;
        }

        public void Push(HexNode tilePosition)
        {
            _nodes.Add(tilePosition);
        }

        public Path ToPath()
        {
            var path = new Queue<Position>();
            for(var i = _nodes.Count - 1; i >= 0; i--)
                path.Enqueue(_nodes[i].ToPosition());

            return new Path(path);
        }

        public Route Reverse()
        {
            var route = new Route(new List<HexNode>());
            for (var i = _nodes.Count - 1; i >= 0; i--)
                route.Push(_nodes[i]);

            return route;
        }

        public IEnumerator<HexNode> GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}