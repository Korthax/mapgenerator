﻿namespace MapGenerator.Map
{
    public enum NodeType : uint
    {
        None = 0,
        // Liquid
        Water = 1,
        Ocean = 2,
        Lake = 3,
        DeepOcean = 4,
        Ice = 5,

        // Land
        Grassland = 50,
        Coast = 51,
        Forest = 52,
        Dirt = 53,

        // Raised
        Mountains = 75,
        Hill = 76
    }
}