﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MapGenerator.Rendering
{
    public static class Colour
    {
        public static readonly Dictionary<int, Color> Colours;
        private const int Centre = 128;
        private const int Width = 127;
        private const double Frequency = 2.4;

        static Colour()
        {
            Colours = new Dictionary<int, Color>
            {
                { 0, new Color(255, 255, 255) }
            };

            for (var i = 1; i < 50; ++i)
            {
                var red = (float)Math.Sin(Frequency * i + 0) * Width + Centre;
                var green = (float)Math.Sin(Frequency * i + 2) * Width + Centre;
                var blue = (float)Math.Sin(Frequency * i + 4) * Width + Centre;
                Colours.Add(i, new Color((byte)red, (byte)green, (byte)blue));
            }
        }

        public static Color For(int value)
        {
            while (value > Colours.Count - 1)
                value -= Colours.Count;

            return Colours[value];
        }
    }
}