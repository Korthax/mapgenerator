﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapGenerator.Rendering
{
    public class ContentFactory
    {
        private readonly Dictionary<string, Texture2D> _textures;
        private readonly Dictionary<string, SpriteFont> _fonts;
        private readonly ContentManager _contentManager;

        public static ContentFactory From(GraphicsDevice graphicsDevice, ContentManager contentManager)
        {
            var blankTexture = new Texture2D(graphicsDevice, 1, 1);
            blankTexture.SetData(new[] { Color.White });

            var fonts = new Dictionary<string, SpriteFont>
            {
                { "default", contentManager.Load<SpriteFont>("Fonts/default") }
            };

            return new ContentFactory(contentManager, blankTexture, fonts);
        }

        private ContentFactory(ContentManager contentManager, Texture2D blankTexture, Dictionary<string, SpriteFont> fonts)
        {
            _contentManager = contentManager;
            _fonts = fonts;
            _textures = new Dictionary<string, Texture2D>
            {
                { "", blankTexture },
            };
        }

        public Texture2D Blank()
        {
            return _textures[""];
        }

        public Texture2D Load(string name)
        {
            if (!_textures.ContainsKey(name))
                _textures.Add(name, _contentManager.Load<Texture2D>(name));

            return _textures[name];
        }

        public SpriteFont Font()
        {
            return _fonts["default"];
        }
    }
}