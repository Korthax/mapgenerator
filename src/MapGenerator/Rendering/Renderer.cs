﻿using MapGenerator.Types;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MapGenerator.Rendering
{
    public class Renderer
    {
        private readonly ContentFactory _contentFactory;
        private readonly SpriteBatch _spriteBatch;
        private readonly Camera _camera;

        public Renderer(SpriteBatch spriteBatch, ContentFactory contentFactory, Camera camera)
        {
            _spriteBatch = spriteBatch;
            _contentFactory = contentFactory;
            _camera = camera;
        }

        public void Begin()
        {
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, _camera.GetViewMatrix());
        }

        public void Render(Rectangle rectangle, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Blank(), rectangle, color);
        }

        public void RenderBlank(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Blank(), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void RenderDot(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/dot"), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void RenderBlob(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/blob"), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void RenderCircle(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/circle"), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void RenderOutline(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/outline"), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void RenderOutline(Position position, Position origin, Size size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/outline"), new Rectangle((int)position.X, (int)position.Y, (int)size.Width, (int)size.Height), null, color, 0, new Vector2(origin.X, origin.Y), SpriteEffects.None, 0);
        }

        public void Render(Position position, IndexSize size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/debug"), new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height), color);
        }

        public void Render(Position position, Position origin, Size size, Color color)
        {
            _spriteBatch.Draw(_contentFactory.Load("Tiles/debug"), new Rectangle((int)position.X, (int)position.Y, (int)size.Width, (int)size.Height), null, color, 0, new Vector2(origin.X, origin.Y), SpriteEffects.None, 0);
        }

        public void Render(string textureName, Position position, IndexSize size, float rotation)
        {
            var rectangle = new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height);
            _spriteBatch.Draw(_contentFactory.Load(textureName), rectangle, null, Color.White, rotation, Vector2.Zero, SpriteEffects.None, 1);
        }

        public void Render(string textureName, Position position, IndexSize size, Position sourcePosition, IndexSize sourceSize, float rotation)
        {
            var rectangle = new Rectangle((int)position.X, (int)position.Y, size.Width, size.Height);
            var soureceRectangle = new Rectangle((int)sourcePosition.X, (int)sourcePosition.Y, sourceSize.Width, sourceSize.Height);
            _spriteBatch.Draw(_contentFactory.Load(textureName), rectangle, soureceRectangle, Color.White, rotation, Vector2.Zero, SpriteEffects.None, 1);
        }

        public void End()
        {
            _spriteBatch.End();
        }

        public void WriteText(string value)
        {
            _spriteBatch.DrawString(_contentFactory.Font(), value, Vector2.Zero, Color.White);
        }
    }
    public class UiRenderer
    {
        private readonly ContentFactory _contentFactory;
        private readonly SpriteBatch _spriteBatch;

        public UiRenderer(SpriteBatch spriteBatch, ContentFactory contentFactory)
        {
            _spriteBatch = spriteBatch;
            _contentFactory = contentFactory;
        }

        public void Begin()
        {
            _spriteBatch.Begin();
        }

        public void WriteText(string value, Vector2 position)
        {
            _spriteBatch.DrawString(_contentFactory.Font(), value, position, Color.White);
        }

        public void End()
        {
            _spriteBatch.End();
        }
    }
}