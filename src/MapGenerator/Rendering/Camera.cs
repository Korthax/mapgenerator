﻿using System;
using System.Reactive.Disposables;
using MapGenerator.Input;
using MapGenerator.Types;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MapGenerator.Rendering
{
    public class Camera : IDisposable
    {
        private readonly CompositeDisposable _disposable;
        private readonly Vector2 _origin;

        public Vector2 Position;
        public float Zoom;

        public Camera(Viewport viewport)
        {
            _origin = new Vector2(viewport.Width / 2.0f, viewport.Height / 2.0f);
            Zoom = 1.0f;
            Position = Vector2.Zero;
            _disposable = new CompositeDisposable();
        }

        public void BindTo(InputDetector inputDetector)
        {
            _disposable.Add(inputDetector.OnMouseMoved.Subscribe(LookAt));
        }

        public Matrix GetViewMatrix()
        {
            return Matrix.CreateTranslation(new Vector3(-Position, 0.0f));
        }

        public void LookAt(MouseMoveEvent moveEvent)
        {
            Position = moveEvent.MousePosition.ToVector2();
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}