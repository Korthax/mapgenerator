﻿using System;

namespace MapGenerator
{
    public class RandomGenerator
    {
        public int Seed { get; }

        private readonly Random _random;

        public RandomGenerator(int seed)
        {
            Seed = seed;
            _random = new Random(seed);
        }

        public int Next(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}