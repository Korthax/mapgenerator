﻿using System;
using System.Threading;

namespace MapGenerator
{
    public class DebugConfiguration
    {
        private static readonly Lazy<DebugConfiguration> Instance = new Lazy<DebugConfiguration>(LazyThreadSafetyMode.ExecutionAndPublication);
        public static DebugConfiguration Value => Instance.Value;

        public bool RenderGatewayNodes { get; set; }
        public bool RenderGatewayNeighbours { get; set; }
        public bool RenderGatewayPaths { get; set; }
        public bool RenderRegions { get; set; }
        public bool RenderClearance { get; set; }
    }
}