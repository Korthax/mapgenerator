﻿using System.Collections.Generic;

namespace MapGenerator.Pipelines
{
    public class PipelineBuilder<T> : IPipelineBuilder<T>
    {
        private readonly LinkedList<IMiddleware<T>> _middleware;

        public PipelineBuilder()
        {
            _middleware = new LinkedList<IMiddleware<T>>();
        }

        public PipelineBuilder<T> AddToEnd(IMiddleware<T> middleware)
        {
            _middleware.AddLast(middleware);
            return this;
        }

        public IPipeline<T> Build()
        {
            return Pipeline<T>.From(_middleware);
        }
    }
}