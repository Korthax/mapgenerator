﻿namespace MapGenerator.Pipelines
{
    public interface IMiddleware<T>
    {
        void Process(IPipeline<T> pipeline, T context);
    }
}