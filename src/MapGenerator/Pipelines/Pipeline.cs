﻿using System.Collections.Generic;
using System.Linq;

namespace MapGenerator.Pipelines
{
    public class Pipeline<T> : IPipeline<T>
    {
        private readonly IPipeline<T> _current;

        public static Pipeline<T> From(IEnumerable<IMiddleware<T>> middleware)
        {
            var reversedMiddleware = new Queue<IMiddleware<T>>(middleware.Reverse());
            if (reversedMiddleware.Count == 0)
                return new Pipeline<T>(new PipelineEnd());

            IPipeline<T> current = new PipelineEnd();
            while(reversedMiddleware.Count > 0)
                current = new PipelinePiece(current, reversedMiddleware.Dequeue());

            return new Pipeline<T>(current);
        }

        private Pipeline(IPipeline<T> current)
        {
            _current = current;
        }

        public void Next(T pipelineContex)
        {
            _current.Next(pipelineContex);
        }

        private class PipelineEnd : IPipeline<T>
        {
            public void Next(T pipelineContex)
            {
            }
        }

        private class PipelinePiece : IPipeline<T>
        {
            private readonly IMiddleware<T> _middleware;
            private readonly IPipeline<T> _next;

            public PipelinePiece(IPipeline<T> next, IMiddleware<T> middleware)
            {
                _next = next;
                _middleware = middleware;
            }

            public void Next(T pipelineContex)
            {
                _middleware.Process(_next, pipelineContex);
            }
        }
    }

    public class NullPipeline<T> : IPipeline<T>
    {
        public void Next(T pipelineContex)
        {
        }
    }
}