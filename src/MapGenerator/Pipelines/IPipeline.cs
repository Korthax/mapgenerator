﻿namespace MapGenerator.Pipelines
{
    public interface IPipeline<in T>
    {
        void Next(T pipelineContext);
    }
}