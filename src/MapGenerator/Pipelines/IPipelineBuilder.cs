﻿namespace MapGenerator.Pipelines
{
    public interface IPipelineBuilder<T>
    {
        PipelineBuilder<T> AddToEnd(IMiddleware<T> middleware);
        IPipeline<T> Build();
    }
}